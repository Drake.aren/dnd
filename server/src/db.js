const fs = require('fs')
const path = require('path')

const db = {
  path: null,
  setPath(filePath) {
    this.path = path.join(__dirname, filePath)
  },
  get() {
    return JSON.parse(fs.readFileSync(this.path))
  },
  write(database) {
    fs.writeFileSync(this.path, database)
  }
}

module.exports = db
