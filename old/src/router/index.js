import Vue from 'vue';
import Router from 'vue-router';
import CharacterList from '@/components/CharacterList';
import History from '@/components/History';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'CharacterList',
      component: CharacterList
    },
    {
      path: '/history',
      name: 'History',
      component: History
    }
  ]
});
