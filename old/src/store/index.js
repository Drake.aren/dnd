import Vue from 'vue';
import Vuex from 'vuex';

import character from './character';
import history from './history';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  modules: {
    history,
    character
  }
});
