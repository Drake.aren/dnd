import { stat } from 'fs'

export default {
  namespaced: true,
  state: {
    races: [
      {
        domain: 'Human',
        subrace: [
          {
            label: 'Human'
          }
        ]
      },
      {
        domain: 'Elf',
        subrace: [
          {
            label: 'Elf'
          },
          {
            label: 'Half-Elf',
            speed: 30,
            charisma: 2,
            cans: [
              {
                label: 'darkvision',
                value: 60,
                domain: 'feet'
              }
            ]
          },
          {
            label: 'High Elf'
          }
        ]
      },
      {
        domain: 'Dwarf',
        subrace: [
          {
            label: 'Hill Dwarf'
          },
          {
            label: 'Dwarf',
            speed: 25,
            cans: [
              {
                label: 'darkvision',
                value: 60,
                domain: 'feet'
              },
              { label: 'poison damage resistance' }
            ],
            constitution: 2
          }
        ]
      },
      {
        domain: 'Halfling',
        subrace: [
          {
            label: 'Halfling'
          },
          {
            label: 'Lightfoot'
          }
        ]
      },
      {
        domain: 'Dragonborn',
        subrace: [
          {
            label: 'Dragonborn',
            speed: 30,
            strength: 2,
            charisma: 1
          }
        ]
      },
      {
        domain: 'Gnome',
        subrace: [
          {
            label: 'Gnome'
          },
          {
            label: 'Rock Gnome'
          }
        ]
      },
      {
        domain: 'Orc',
        subrace: [
          {
            label: 'Half-Orc'
          }
        ]
      },
      {
        domain: 'Tiefling',
        subrace: [
          {
            label: 'Tiefling',
            speed: 30,
            intelligence: 1,
            charisma: 2,
            cans: [
              {
                label: 'fire damage resistance'
              }
            ]
          }
        ]
      }
    ],
    // hit point - helth point
    classes: [
      {
        label: 'Barbarian',
        hitPoint: 'd12'
      },
      {
        label: 'Bard',
        hitPoint: 'd8'
      },
      {
        label: 'Cleric',
        hitPoint: 'd8'
      },
      {
        label: 'Druid',
        hitPoint: 'd8'
      },
      {
        label: 'Fighter',
        hitPoint: 'd10'
      },
      {
        label: 'Monk',
        hitPoint: 'd8'
      },
      {
        label: 'Paladin',
        hitPoint: 'd10'
      },
      {
        label: 'Ranger',
        hitPoint: 'd10'
      },
      {
        label: 'Rogue',
        hitPoint: 'd8'
      },
      {
        label: 'Sorcerer',
        hitPoint: 'd8'
      },
      {
        label: 'Warlock',
        hitPoint: 'd8'
      },
      {
        label: 'Wizard',
        hitPoint: 'd6'
      }
    ],
    ability: {
      credit: 27,
      base: 8,
      maxStartValue: 15,
      characterMaxValue: 20,
      monstersMaxValue: 30,
      checks: [
        {
          label: 'Very easy',
          value: 5
        },
        {
          label: 'Easy',
          value: 10
        },
        {
          label: 'Medium',
          value: 15
        },
        {
          label: 'Hard',
          value: 20
        },
        {
          label: 'Very hard',
          value: 25
        },
        {
          label: 'Nearly impossible',
          value: 30
        }
      ],
      list: [
        {
          label: 'strength'
        },
        {
          label: 'dexterity'
        },
        {
          label: 'constitution'
        },
        {
          label: 'intelligence'
        },
        {
          label: 'wisdom'
        },
        {
          label: 'charisma'
        }
      ]
    },
    players: [
      {
        name: 'Anna',
        race: 1,
        subrace: 1,
        height: 175,
        weight: 45,
        age: 15,
        classes: [8, 2],
        abilities: {
          strength: 8,
          dexterity: 8,
          constitution: 8,
          intelligence: 8,
          wisdom: 8,
          charisma: 8
        },
        equipment: []
      },
      {
        name: 'Anna',
        race: 1,
        subrace: 1,
        height: 175,
        weight: 45,
        age: 15,
        classes: [8, 2],
        abilities: {
          strength: 8,
          dexterity: 8,
          constitution: 8,
          intelligence: 8,
          wisdom: 8,
          charisma: 8
        },
        equipment: []
      }
    ]
  },
  getters: {
    getRaces: state => {
      let result = []
      state.races.forEach(e => {
        result.push(e.domain)
      })
      return result
    },
    getSubraces: state => raceIndex => {
      let result = []
      state.races[raceIndex].subrace.forEach(e => {
        result.push(e)
      })
      return result
    },
    getRace: state => raceIndex => {
      return state.races[raceIndex].domain
    },
    getCharacterData: (state, { getRace }) => playerID => {
      let player = state.players[playerID]
      let subrace = state.races[player.race].subrace[player.subrace]
      let abilities = state.ability.list.map(({ label }) => {
        let raceAbility = subrace[label] === undefined ? 0 : subrace[label]
        return player.abilities[label] + raceAbility
      })
      return {
        ...player,
        race: getRace(player.race),
        subrace,
        classes: player.classes.map(e => {
          return state.classes[e]
        }),
        abilities
      }
    },
    checkAbility: state => ({ playerID, abilityID, d20, value }) => {
      let player = state.players[playerID]
      let ability = state.ability.list[abilityID].label
      let abilityScore = player.abilities[ability]
      let modificator = Math.round((abilityScore - 10) / 2)
      return d20 + modificator >= value
    }
  }
}
