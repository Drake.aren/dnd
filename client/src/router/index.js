import Vue from 'vue'
import Router from 'vue-router'
import CharactersInfo from '@/pages/CharactersInfo'
import Login from '@/pages/Login'
import AddItem from '@/components/AddItem'
import CharacterInfo from '@/components/CharacterInfo'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Characters Info',
      component: CharactersInfo
    },
    {
      path: '/info/:item/:name',
      name: 'Character Info',
      showInMenuBar: false,
      component: CharacterInfo
    },
    {
      path: '/add-item',
      name: 'Add item',
      component: AddItem
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    }
  ]
})
